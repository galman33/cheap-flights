var http = require("http");

function getJSON(options, onResult)
{
    console.log("rest::getJSON");

    var prot = options.port == 443 ? https : http;
    var req = prot.request(options, function(res)
    {
        var output = '';
        console.log(options.host + ':' + res.statusCode);
        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            output += chunk;
        });

        res.on('end', function() {
            var obj = JSON.parse(output);
            onResult(res.statusCode, obj);
        });
    });

    req.on('error', function(err) {
        //res.send('error: ' + err.message);
    });

    req.end();
};

var options = {
    host: 'partners.api.skyscanner.net',
    port: 80,
    //path: '/apiservices/browsedates/v1.0/IL/ILS/en-GB/TLV-sky/PARI-sky/2016-05/2016-05?apiKey=prtl6749387986743898559646983194',
    path: '/apiservices/browsequotes/v1.0/IL/ILS/en-GB/TLV-sky/anywhere/anytime/anytime?apiKey=prtl6749387986743898559646983194',
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
};

getJSON(options, (status, res) => { 
    var directFlights = res.Quotes.filter((quote) => quote.Direct)
    var sorted = directFlights.sort((a, b) => parseInt(a.MinPrice) - parseInt(b.MinPrice))
    var placesCache = []
    for(var place of res.Places)
        placesCache[place.PlaceId] = place.CityName + ', ' + place.CountryName
        
    var endResults = []
    for(var quote of sorted)
        endResults.push({
            a_fromPlace: placesCache[quote.OutboundLeg.OriginId],
            b_toPlace: placesCache[quote.OutboundLeg.DestinationId],
            c_from: quote.OutboundLeg.DepartureDate,
            d_to: quote.InboundLeg.DepartureDate,
            e_price: quote.MinPrice
        })
    debugger;
})